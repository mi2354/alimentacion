#!/usr/bin/python
# -*- coding: utf-8 -*-
import mysql.connector as conector

db = conector.connect(host="localhost",    # your host, usually localhost
                     user="miguel",         # your username
                     passwd="contra123",  # your password
                     database="alimentacion")        # name of the data base
# you must create a Cursor object. It will let
#  you execute all the queries you need
if db.is_connected():
    print('Connected to MySQL database')
cur = db.cursor()


def ver_recetas(ingredientes):
    n = len(ingredientes)
    res = set()
    if n == 1:
        cur.execute("SELECT nombre_receta FROM recetas WHERE ingrediente = '" + ingredientes[0] +"';")
        #db.commit() # ------------------------------
        for receta in cur.fetchall():
            res.add(receta[0])
    else:
        aux = {}
        for ingrediente in ingredientes:
            #print(ingrediente)
            cur.execute("SELECT nombre_receta FROM recetas WHERE ingrediente = '" + ingrediente +"';")
            #db.commit() # ------------------------------
            for receta in cur.fetchall():
                if receta[0] not in aux:
                    aux[receta[0]] = 1
                else:
                    aux[receta[0]] +=1
        for receta in aux:
            if aux[receta] == n:
                res.add(receta)
    return res

def anadir_receta(nombre, ingredientes):
    for ingrediente in ingredientes:
        cur.execute("INSERT INTO alimentacion.recetas VALUES ('" + \
        ingrediente + "', '" + nombre +"');")
        db.commit() # ------------------------------

def ver_ingredientes(nombre):
    cur.execute("SELECT ingrediente FROM alimentacion.recetas WHERE nombre_receta = '" + nombre +"';")
    #db.commit() # ------------------------------
    res = set()
    for receta in cur.fetchall():
        #print("- " + receta[0])
        res.add(receta[0])
    return res

def ver_lista_recetas():
    cur.execute("SELECT DISTINCT nombre_receta FROM alimentacion.recetas;")
    #db.commit() # ------------------------------
    res = set()
    for receta in cur.fetchall():
        #print("- " + receta[0])
        res.add(receta[0])
    return res


