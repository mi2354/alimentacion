#!/usr/bin/python
# -*- coding: utf-8 -*-
import conexion2 as conexion

def comienzo():
    x = input('¡Bienvenido/a!, ¿qué desea hacer? Pulse el numero correspondiente \n\n' + \
    '1. Descubrir que puedo cocinar con un alimento \n' + \
    '2. Ver que alimentos lleva mi receta \n' + \
    '3. Ver lista de recetas \n' + \
    '4. Introduir una receta \n\n')
    return x


def volver_a_empezar():
    z = input('¿Desea volver a empezar? (s/n): ')
    z = z.lower()
    if z == 's' or z == 'si':
        return(False)
    else:
        return(True)


try:
    correcto1 = False
    while not correcto1:
        x = comienzo()
        if x == '1':
            correcto1 = True
            y = input('Introduzca los alimentos de los que desea buscar una receta separados por comas:\n\n')
            y = y.lower()
            y_splited = y.split(',')
            enviar = []
            for i in y_splited:
                s = i.strip(' ')
                s = s.replace(' ', '_')
                enviar.append(s)
            res = conexion.ver_recetas(enviar)
            if len(res) > 0:
                print('Puede preparar:')
                for receta in res:
                    print( '- '+ receta )
            else:
                print('No hay recetas con estos ingredientes')
            
            correcto1 = volver_a_empezar()
            
        elif x == '2':
            correcto1 = True
            y = input('Introduzca el nombre de la receta:\n\n')
            enviar = y.strip(' ').lower()
            enviar = enviar.replace(' ', '_')
            res = conexion.ver_ingredientes(enviar)
            for receta in res:
                    print( '- '+ receta.replace('_',' '))
            
            correcto1 = volver_a_empezar()
        elif x == '3':
            correcto1 = True
            res = conexion.ver_lista_recetas()
            for receta in res:
                print( '- '+ receta.replace('_',' '))
            correcto1 = volver_a_empezar()
        elif x == '4':
            nombre = input('Introduzca el nombre de la receta: \n\n')
            nombre = nombre.strip(' ').lower()
            nombre = nombre.replace(' ', '_')
            y = input('Introduzca los ingredientes de la receta: \n\n')
            y_splited = y.split(',')
            ingredientes = []
            for i in y_splited:
                s = i.strip(' ').lower()
                s = s.replace(' ', '_')
                ingredientes.append(s)
            conexion.anadir_receta(nombre, ingredientes)
            correcto1 = volver_a_empezar()
        else:
            print('Introduzca un numero del 1 al 4')
except:
    print('puta bida, algo va mal xD')
